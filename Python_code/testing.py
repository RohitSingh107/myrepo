from guizero import App, MenuBar, Box, TextBox, PushButton

# These are the functions that are called by selecting options from each submenu
def open_file():
    with open(file_name.value, "r") as f:
        editor.value = f.read()

def save_file():
    with open(file_name.value, "w") as f:
        f.write(editor.value)
        save_button.disable()

def enable_save():
    save_button.enable()

# A new function that closes the app
def exit_app():
    app.destroy()

def clear_text():
    global temp_text
    temp_text = editor.value
    editor.value = ""

def restore_text():
    editor.value = temp_text

app = App()

menubar = MenuBar(app,
                  # These are the menu options
                  toplevel=["File", "Edit"],
                  # The options are recorded in a nested lists, one list for each menu option
                  # Each option is a list containing a name and a function
                  options=[
                      [["Open", open_file], ["Save", save_file], ["Exit", exit_app]],
                      [["Clear Text", clear_text], ["Restore Text", restore_text]]
                  ])
file_controls = Box(app, align="top", width="fill", border=True)
file_name = TextBox(file_controls, text="text_file.txt", width=50, align="left")

save_button = PushButton(file_controls, text="Save", command=save_file, align="right")
open_button = PushButton(file_controls, text="Open", command=open_file, align="right")

editor = TextBox(app, multiline=True, height="fill", width="fill", command=enable_save)

# This is where your additional features go
app.display()